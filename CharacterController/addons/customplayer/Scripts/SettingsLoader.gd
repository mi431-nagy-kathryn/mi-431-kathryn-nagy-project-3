extends Node

@export var kinematic_controller_settings: KinematicControllerSettings

@export var gravity_settings: GravitySettings
@export var kinematic_controller_path: NodePath
@onready var kinematic_controller = get_node(kinematic_controller_path)

@export var rotator_settings: RotatorSettings
@export var rotator_path: NodePath
@onready var rotator = get_node(rotator_path)

@export var combos: Array[Combo]

# Called when the node enters the scene tree for the first time.
func _ready():
	if (kinematic_controller.has_method("load_kinematic_settings") and kinematic_controller_settings != null):
		kinematic_controller.load_kinematic_settings(kinematic_controller_settings)
		if (combos.size() > 0):
			kinematic_controller.load_combos(combos)
	if (kinematic_controller.has_method("load_gravity_settings") and gravity_settings != null):
		kinematic_controller.load_gravity_settings(gravity_settings)
	if (rotator.has_method("load_rotator_settings") and rotator_settings != null):
		rotator.load_rotator_settings(rotator_settings)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
