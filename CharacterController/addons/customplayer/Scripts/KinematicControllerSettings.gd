extends Resource
class_name KinematicControllerSettings

@export_category("Kinematic Character Controller")
@export_group("Ground Speed and Acceleration")
#Exported variables for walking-type motion (as opposed to jumping)
@export var speed = 500.0 ##Maximum speed for walking in pixels/second
@export var acceleration: Curve ##If you want to define a custom acceleration curve here, this is where to do it.
@export var acceleration_length = 0.5 ##How long it takes to reach maximum speed in seconds
@export var deceleration_length = 0.25 ##How long it takes to stop from maximum speed in seconds
@export var turn_around_factor: float = 1.0 ##If 1.0, inverting direction will be faster. If -1.0, inverting direction will be slower. In-between values correspond.

@export_group("Input for Ground Movement")
#Each entry is a Vector representing positive motion in a given axis of motion. It would be good practice to normalize these, but it is not necessary.
@export var motion_basis_vectors: Array = [Vector2(1.0, 0.0), Vector2(0.0, 1.0)]
#Entries correspond with motion_basis_vectors. Each array of strings has the name of the action for negative motion in that axis at index 0, and the name of the action for positive motion at index 1.
@export var axis_actions = [["move_left_2D", "move_right_2D"], ["ui_up", "ui_down"]] # (Array, Array, String)
#If this is true, calculations are done in 3D space, otherwise, they are done in 2D space.
#export(bool) var use_third_dimension = false
#Variables for controller handling
@export var input_action_names = ["jump"] # (Array, String)




@export_group("Automatic Rotation")
#Variables for rotation.
@export var rotate_motion_match_transform = true
@export var rotate_motion_match_camera = false
@export_group("")


@export_group("Physics")
@export_subgroup("Physics Material Settings")
#Default physics material
@export var default_bounce_threshold = 500.0
@export var bounce_time_factor = 0.2
@export var default_bounciness = 0.0 #The factor by which the player's velocity is reflected.
@export var default_static_friction = 1.0 #The friction that the player experiences when starting to move from a standstill. Note: Should be at least 0.25. For slipperier surfaces, lower only the dynamic friction.
@export var default_dynamic_friction = 1.0 #The friction that the player experiences when changing direction while moving.


@export_subgroup("Default Values for move_and_slide_with_snap() Physics")
@export var stop_on_slope = false
@export var max_slides = 4
@export var floor_max_angle = 0.785398
@export var infinite_inertia = true
@export_group("")


#Variables for creating combos
@export_group("Combo Input and Settings")
@export var combo_reset_time: float = 0.5
@export var combo_actions = [["rotate_left_2D", "rotate_right_2D"]] #The actions that can be registered in combos. # (Array, Array, String)
@export var possible_combos: = [[]] #A 2D array consisting of combos. A combo is an array of arrays, representing a sequence of actions, subarrays contain multiple actions that must be active simulatneously. # (Array, Array, Resource)
#export(Array, bool) var combo_strictness #If true, a strict equality is needed for this combo. Otherwise, each part must only be a subarray of the defined combo's corresponding part.
