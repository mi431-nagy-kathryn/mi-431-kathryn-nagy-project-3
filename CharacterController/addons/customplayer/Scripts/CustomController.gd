extends Node

@export var kinematic_controller_settings: KinematicControllerSettings
@export var combos: Array[Combo]
@export var gravity_settings: GravitySettings
@export var rotator_settings: Array[RotatorSettings]
@export var controller_path: NodePath
@export var rotator_paths: Array[NodePath]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
