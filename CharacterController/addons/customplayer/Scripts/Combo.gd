extends Resource
class_name Combo

@export var actions = [[""]] # (Array, Array, String)
@export var function_name: String
@export var use_strict_equality_instant: bool
@export var use_strict_equality_actions: bool
#export(bool) var use_strict_equality_instant
#export(bool) var use_strict_equality_actions


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
