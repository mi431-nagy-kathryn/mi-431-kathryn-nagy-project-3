@tool
extends Node2D

var arc: PackedVector2Array

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _draw():
	var coords = generate_coords(Vector2(0, 0), 100, 100)
	for i in range(0, coords.size() - 2):
		draw_line(coords[i], coords[i+1], Color.CYAN, 1)

func generate_coords(start_pos: Vector2, height, start_width):
	return [start_pos + Vector2(0, 0), start_pos + Vector2(0.25 * start_width, 0.4375 * height), start_pos + Vector2(0.5 * start_width, 0.75 * height), Vector2(0.75 * start_width, 0.9375 * height)]
