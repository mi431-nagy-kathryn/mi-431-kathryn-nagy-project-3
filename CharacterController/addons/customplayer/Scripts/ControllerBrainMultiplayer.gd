extends Node

@export var controller_path: NodePath
@onready var controller = get_node(controller_path)
@export var actions_to_register = ["jump"] # (Array, String)


# Called when the node enters the scene tree for the first time.
func _ready():
	if !controller.has_method("set_axis_inputs_state"):
		print("Error: The controller specified by controller_path is not a valid KinematicController.")


The master and mastersync rpc behavior is not officially supported anymore. Try using another keyword or making custom logic using get_multiplayer().get_remote_sender_id()
@rpc func _physics_process(delta):
	controller.set_axis_inputs_state()
	for i in actions_to_register:
		controller.set_button_inputs_state([i])

The master and mastersync rpc behavior is not officially supported anymore. Try using another keyword or making custom logic using get_multiplayer().get_remote_sender_id()
@rpc func _input(event):
	controller.register_all_combo_actions()
	controller.combo_reset_timer = 0.0
