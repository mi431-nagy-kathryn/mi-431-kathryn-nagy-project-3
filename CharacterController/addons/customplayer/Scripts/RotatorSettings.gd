extends Resource
class_name RotatorSettings

#Variables for rotation
##@export_category("Manual Rotation")
##@export_group("Node Paths")
##@export var kinematic_body_path: NodePath
##@export var rotation_target_path: NodePath
##@export var controller_path: NodePath
@export_group("Rotation Input")
enum InputMode {SINGLE_INPUT, DUAL_INPUT}
@export var input_mode: InputMode
#Positive always rotates in one direction, negative always rotates in opposite.
@export var positive_rotation_action: String
@export var negative_rotation_action: String
#Alternative - there are two axes, and rotation target is specified as an angle to the specified value in the plane.
@export var multi_axis_rotation_actions = [[], []] # First index is x or y axis, second index is negative of positive input # (Array, Array, String)
enum RotationMode {LOOK_TO_POINT, MATCH_POINT_ORIENTATION}
#export var is_movement_rotated = false
@export var rotation_mode: RotationMode = RotationMode.MATCH_POINT_ORIENTATION
@export_group("Rotation Settings")
@export var rotation_speed: float = 10.0
@export var rotation_interpolation: Curve
@export var rotation_interpolation_time = 1.0
@export var rotation_deceleration_time = 0.5
@export var rotation_offset = 1.5708
@export var use_local_target_position: bool = true
@export_subgroup("3D Rotation Only")
@export var rotation_axis: Vector3 = Vector3(0, 1, 0) #Only used in 3D calculations - the axis about which rotation occurs when applying the rotation offset.
@export var up_axis: Vector3 = Vector3(0, 1, 0) #Only used in 3D calculations
