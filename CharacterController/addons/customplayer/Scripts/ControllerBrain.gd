extends Node
class_name ControllerBrain

@export var controller_path: NodePath
@onready var controller = get_node(controller_path)
@export var actions_to_register = ["jump"] # (Array, String)
@export var rotator_paths = [] # (Array, NodePath)
@onready var rotators = []

var use_joystick_input = false #If true, uses raw input values for axes, ideal for joystick; if false, uses -1, 0, or 1, ideal for button presses.

# Called when the node enters the scene tree for the first time.
func _ready():
	if !controller.has_method("set_axis_inputs_state"):
		print("Error: The controller specified by controller_path is not a valid KinematicController.")
	for i in rotator_paths:
		rotators.append(get_node(i))

func _physics_process(delta):
	if use_joystick_input:
		controller.set_axis_inputs_state_joystick()
	else:
		controller.set_axis_inputs_state()
	for i in actions_to_register:
		controller.set_button_inputs_state([i])
	for i in rotators:
		i.set_inputs_state()
	

func _input(event):
	controller.register_all_combo_actions()
	controller.combo_reset_timer = 0.0
	
