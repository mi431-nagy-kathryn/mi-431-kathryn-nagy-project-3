extends Node
class_name KinematicController

#Kinematic Body being controlled (MOVE THIS TO BASE SCRIPT)


#Exported variables with more complex variable types


@export_category("Base Character Controller")
@export_group("Ground Speed and Acceleration")
#Exported variables for walking-type motion (as opposed to jumping)
@export var speed = 500.0 ##Maximum speed for walking in pixels/second
@export var acceleration: Curve ##If you want to define a custom acceleration curve here, this is where to do it.
@export var acceleration_length = 0.5 ##How long it takes to reach maximum speed in seconds
@export var deceleration_length = 0.25 ##How long it takes to stop from maximum speed in seconds
@export var turn_around_factor: float = 1.0 ##If 1.0, inverting direction will be faster. If -1.0, inverting direction will be slower. In-between values correspond.

@export_group("Input for Ground Movement")
#Each entry is a Vector representing positive motion in a given axis of motion. It would be good practice to normalize these, but it is not necessary.
@export var motion_basis_vectors: Array = [Vector2(1.0, 0.0), Vector2(0.0, 1.0)]
#Entries correspond with motion_basis_vectors. Each array of strings has the name of the action for negative motion in that axis at index 0, and the name of the action for positive motion at index 1.
@export var axis_actions = [["move_left_2D", "move_right_2D"], ["ui_up", "ui_down"]] # (Array, Array, String)
#If this is true, calculations are done in 3D space, otherwise, they are done in 2D space.
#export(bool) var use_third_dimension = false
#Variables for controller handling
var input_actions = [] #[][0] is pressed, [][1] is just_pressed, [][2] is just_released
@export var input_action_names = ["jump"] # (Array, String)
var input_action_dictionary = {}



@export_group("Automatic Rotation")
#Variables for rotation.
@export var rotate_motion_match_transform = true
@export var rotate_motion_match_camera = false
@export_group("")


@export_group("Physics")
@export_subgroup("Physics Material Settings")
#Default physics material
@export var default_bounce_threshold = 500.0
@export var bounce_time_factor = 0.2
@export var default_bounciness = 0.0 #The factor by which the player's velocity is reflected.
@export var default_static_friction = 1.0 #The friction that the player experiences when starting to move from a standstill. Note: Should be at least 0.25. For slipperier surfaces, lower only the dynamic friction.
@export var default_dynamic_friction = 1.0 #The friction that the player experiences when changing direction while moving.


@export_subgroup("Default Values for move_and_slide_with_snap() Physics")
@export var stop_on_slope = false
@export var max_slides = 4
@export var floor_max_angle = 0.785398
@export var infinite_inertia = true
@export_group("")


#Variables for creating combos
@export_group("Combo Input and Settings")
@export var combo_reset_time: float = 0.5
@export var combo_actions = [["rotate_left_2D", "rotate_right_2D"]] #The actions that can be registered in combos. # (Array, Array, String)
@export var possible_combos: = [[]] #A 2D array consisting of combos. A combo is an array of arrays, representing a sequence of actions, subarrays contain multiple actions that must be active simulatneously. # (Array, Array, Resource)
#export(Array, bool) var combo_strictness #If true, a strict equality is needed for this combo. Otherwise, each part must only be a subarray of the defined combo's corresponding part.

@export_group("Node Path Connections")
@export var kinematic_body_path: NodePath #The path to the kinematic body being controlled
@onready var kinematic_body = get_node(kinematic_body_path)
@export var camera_path: NodePath #Used only for camera transform
#Variables for animation
@export var animation_player_path: NodePath
@export var animation_tree_path: NodePath
#onready var animation_player = get_node(animation_player_path)
#onready var animation_tree = get_node(animation_tree_path)
#onready var animation_node = animation_tree.get("parameters/playback")
@export_group("")



@onready var camera = get_node(camera_path)

#Variables to temporarily adjust motion - these have setters for use cases such as power-ups.
var speed_modifier = 1.0 #Higher values will increase maximum speed, which will be interpolated to when speed boosting.
var friction_modifier = 1.0 #Lower values will reduce friction
var time_scale_modifier = 1.0 #Lower values will slow down time

#Internal variables
var base_velocity # The base velocity, with a maximum magnitude of 1. This is also before curve interpolation is applied.
#var gravitational_velocity = 0.0 # The velocity in the direction of gravity, for both gravity and jumping.
var axes # The number of axes of motion
var current_animation

var previous_frame_velocity

var axis_inputs = []
var boost_input = false
var dimensions

var impulse_array = []
var impulse_time_array = []

var combos = [[]]
var combo_reset_timer

var last_physics_material

var last_collision_normal


func load_kinematic_settings(settings: KinematicControllerSettings):
	speed = settings.speed
	acceleration = settings.acceleration
	acceleration_length = settings.acceleration_length
	deceleration_length = settings.deceleration_length
	turn_around_factor = settings.turn_around_factor
	
	motion_basis_vectors = settings.motion_basis_vectors
	axis_actions = settings.axis_actions
	input_action_names = settings.input_action_names
	
	rotate_motion_match_camera = settings.rotate_motion_match_camera
	rotate_motion_match_transform = settings.rotate_motion_match_transform
	
	default_bounce_threshold = settings.default_bounce_threshold
	bounce_time_factor = settings.bounce_time_factor
	default_bounciness = settings.default_bounciness
	default_static_friction = settings.default_static_friction
	default_dynamic_friction = settings.default_dynamic_friction
	
	stop_on_slope = settings.stop_on_slope
	max_slides = settings.max_slides
	floor_max_angle = settings.floor_max_angle
	infinite_inertia = settings.infinite_inertia
	

func load_combos(combos: Array[Combo]):
	for combo in combos:
		possible_combos.append(combo)

# Called when the node enters the scene tree for the first time.
# Initialization and Error Handling
func _ready():
	print("Starting ready in KinematicController")
	initialize_motion_vectors()
	
#	while len(combo_strictness) < len(possible_combos):
#		combo_strictness.append(true)
	
	call_upon_ready()

func initialize_motion_vectors():
	axes = len(motion_basis_vectors)
	for i in range(len(motion_basis_vectors) - 1):
		if typeof(motion_basis_vectors[0]) != typeof(motion_basis_vectors[i]):
			print("Warning: The motion basis vectors do not all have the same variable type - please fix this.")
	match(typeof(motion_basis_vectors[0])):
		TYPE_VECTOR2:
			base_velocity = Vector2(0, 0)
			dimensions = 2
		TYPE_VECTOR3:
			base_velocity = Vector3(0, 0, 0)
			dimensions = 3
		TYPE_ARRAY:
			base_velocity = []
			for i in motion_basis_vectors[0]:
				base_velocity.append(0)
			dimensions = base_velocity.size()
		_:
			print("Warning: The motion basis vectors use an unrecognized type. Please add support for this")
	previous_frame_velocity = base_velocity
	for i in range(axes):
		axis_inputs.append(0)
	if (len(motion_basis_vectors) != len(axis_actions)):
		print("Warning: motion_basis_vectors, axis_actions, and axis_enabled disagree on the number of axes of motion (they should all have the same number of entries, with the individual entries in axis_actions being arrays with 2 elements, strings that are the names of the actions corresponding to positive and negative motion in that axis of motion). Defaulting to ", axes, " axes of motion.")
	if (acceleration_length == 0.0):
		print("Setting acceleration_length to 0.01 - it cannot be exactly 0.0, as that would lead to a division by 0 error.")
		acceleration_length = 0.01
	if (deceleration_length == 0.0):
		print("Setting deceleration_length to 0.01 - it cannot be exactly 0.0, as that would lead to a division by 0 error.")
		deceleration_length = 0.01
		
func initialize_actions():
	for i in input_action_names:
		input_actions.append([false, false, false])
	
	for i in range(len(input_action_names)):
		input_action_dictionary[input_action_names[i]] = i

func initialize_physics():
	if default_static_friction == 0.0:
		print("Warning: The default physics material should not have friction values of 0 exactly, to avoid division by 0 errors. Changing to 0.01 for now, but if this does not work, change to a different value.")
		default_static_friction = 0.01
	if default_dynamic_friction == 0.0:
		print("Warning: The default physics material should not have friction values of 0 exactly, to avoid division by 0 errors. Changing to 0.01 for now, but if this does not work, change to a different value.")
		default_dynamic_friction = 0.01
	
	last_physics_material = [default_bounciness, default_bounce_threshold, default_static_friction, default_dynamic_friction]
	previous_frame_velocity = n_dimensional_zeros()
	combo_reset_timer = combo_reset_time

func call_upon_ready():
	pass

func _input(event):
	#Just for testing purposes - in actual implementations, this should be handled by an external script that handles the input.
	#set_axis_inputs_state()
	pass

func _physics_process(delta):
	var scaled_delta = delta * time_scale_modifier
	combo_reset_timer += scaled_delta
	if combo_reset_timer > combo_reset_time:
		for i in combos:
			i.clear()
	if !(self.has_method("on_jump")):
		kinematic_body.set_velocity(calculate_base_velocity(delta))
		kinematic_body.move_and_slide()
		kinematic_body.velocity

#Updates the axis_inputs based on a specific event.
func set_axis_inputs_event(event: InputEvent):
	for i in range(axes):
		#axis_inputs[i] = 0
		if event.is_action_just_pressed(axis_actions[i][0]):
			axis_inputs[i] = -1
			return
		if event.is_action_just_pressed(axis_actions[i][1]):
			axis_inputs[i] = 1
			return
		if event.is_action_released(axis_actions[i][0]):
			axis_inputs[i] = 0
		if event.is_action_released(axis_actions[i][1]):
			axis_inputs[i] = 0
		if event.is_action_pressed(axis_actions[i][0]):
			axis_inputs[i] += -1
		if event.is_action_pressed(axis_actions[i][1]):
			axis_inputs[i] += 1

#Updates the axis_inputs based on the overall input state.
func set_axis_inputs_state():
	for i in range(axes):
		axis_inputs[i] = 0
		if Input.is_action_just_pressed(axis_actions[i][0]):
			axis_inputs[i] = -1
			return
		if Input.is_action_just_pressed(axis_actions[i][1]):
			axis_inputs[i] = 1
			return
		if Input.is_action_pressed(axis_actions[i][0]):
			axis_inputs[i] += -1
		if Input.is_action_pressed(axis_actions[i][1]):
			axis_inputs[i] += 1

#Updates the axis_inputs based on an array with specified values (such as for use with an NPC)
func set_axis_inputs_raw(x : Array):
	if x.size() == axis_inputs.size():
		axis_inputs = x
		return true
	return false

func set_axis_inputs_state_joystick():
	for i in range(axes):
		axis_inputs[i] = Input.get_axis(axis_actions[i][0], axis_actions[i][1])

#Actions is an array of strings, defining the action names for the actions to be updated according to input action names.
#Pressed is an array of bools, stating whether an action is pressed.
func set_button_inputs_raw(actions : Array, pressed : Array):
	for i in range(len(actions)):
		var j = input_action_dictionary[actions[i]]
		input_actions[j][1] = pressed[i] and not input_actions[j][0]
		input_actions[j][2] = not pressed[i] and input_actions[j][0]
		input_actions[j][0] = pressed[i]

#Actions is an array of strings, defining the names for the actions to be updated according to input action names.
func set_button_inputs_state(actions: Array):
	for i in actions:
		var j = input_action_dictionary[i]
		input_actions[j][1] = Input.is_action_just_pressed(i)
		input_actions[j][2] = Input.is_action_just_released(i)
		input_actions[j][0] = Input.is_action_pressed(i)
		

func calculate_base_velocity(delta):
	#Calculating the basic direction of motion
	
	var scaled_delta = delta * time_scale_modifier
	
	var target = n_dimensional_zeros() #This represents the intended direction of motion.
	for i in range(axes):
		target += (motion_basis_vectors[i] * axis_inputs[i])
	target = target.normalized()
	
	#Velocity for walking-type motion
	##Handling the base velocity to determine acceleration vs. deceleration, and adding incremental bits of speed
	var direction_of_change = n_dimensional_zeros()
	for i in range(dimensions):
		direction_of_change[i] = signed_value(target[i] - base_velocity[i])
	var signed_base_velocity = n_dimensional_zeros()
	for i in range(dimensions):
		signed_base_velocity[i] = signed_value(base_velocity[i])
	for i in range(len(axis_inputs)):
		if signed_base_velocity[i] == -1 * axis_inputs[i] and signed_base_velocity[i] != 0:
			signed_base_velocity[i] *= clamp(turn_around_factor, -1.0, 1.0)
	#var speed_change_length = deceleration_length if (direction_of_change + signed_base_velocity == n_dimensional_zeros() and not is_all_zeros(base_velocity)) else acceleration_length
	var speed_change_length
	if (direction_of_change + signed_base_velocity == n_dimensional_zeros() and not is_all_zeros(base_velocity)):
		speed_change_length = deceleration_length / last_physics_material[3]
		#print("Deceleration length: ", deceleration_length, "  Dynamic friction: ", last_physics_material[2], "  Result: ", speed_change_length)
	else:
		speed_change_length = acceleration_length / last_physics_material[2]
		#print("Acceleration length: ", acceleration_length, "  Static friction: ", last_physics_material[1], "  Result: ", speed_change_length)
	base_velocity += direction_of_change * ((friction_modifier * scaled_delta) / speed_change_length)
	for i in range(dimensions):
		###This section of code exists to eliminate the drift that occurs when it is not present.
		if abs(base_velocity[i]) < (friction_modifier * scaled_delta/ speed_change_length):
			base_velocity[i] = 0
	if base_velocity.length() > 1:
		base_velocity = base_velocity.normalized()
		
	#set_animation_motion_parameter(base_velocity)
	
	#Implementing Final Motion Calculations
	##Interpolating the base_velocity in accordance with acceleration curve
	#var target_velocity = abs(base_velocity.x)) * (signed_value(base_velocity.x)), acceleration.interpolate(abs(base_velocity.y)) * signed_value(base_velocity.y)
	var final_velocity = interpolated_acceleration()
	if final_velocity.length() >= speed * speed_modifier:
		final_velocity = final_velocity.normalized() * speed * speed_modifier
	
	for i in range(len(impulse_array)):
		impulse_time_array[i].x -= scaled_delta
		final_velocity += impulse_array[i] * acceleration.sample(impulse_time_array[i].x / impulse_time_array[i].y)
	
	clear_expired_impulses()
	
#	if target != n_dimensional_zeros():
#		animation_node.travel("Walking")
#	else:
#		animation_node.travel("Idle")
	
	#previous_frame_velocity = final_velocity
	
	if rotate_motion_match_transform:
		match dimensions:
			2:
				final_velocity = final_velocity.rotated(kinematic_body.global_rotation)
			3:
				final_velocity = kinematic_body.global_transform.basis * (final_velocity)
	if rotate_motion_match_camera:
		match dimensions:
			2:
				final_velocity = final_velocity.rotated(camera.global_rotation)
			3:
				final_velocity = camera.global_transform.basis * (final_velocity)
	
	return final_velocity

func interpolated_acceleration():
	match dimensions:
		2:
			return Vector2(acceleration.sample(abs(base_velocity.x)) * (signed_value(base_velocity.x)), acceleration.sample(abs(base_velocity.y)) * signed_value(base_velocity.y)) * speed * speed_modifier
		3:
			return Vector3(acceleration.sample(abs(base_velocity.x)) * (signed_value(base_velocity.x)), acceleration.sample(abs(base_velocity.y)) * signed_value(base_velocity.y), acceleration.sample(abs(base_velocity.z)) * (signed_value(base_velocity.z))) * speed * speed_modifier
		_:
			return

func apply_impulse(time : float, impulse):
	if time == 0.0:
		return
	impulse_array.append(impulse)
	impulse_time_array.append(Vector2(time, time))

func apply_bounce_impulse(factor : float):
	if last_collision_normal == null:
		print("No valid last_collision_normal")
		return
	apply_impulse(bounce_time_factor * previous_frame_velocity.length() / speed, previous_frame_velocity * last_collision_normal * factor)

func clear_expired_impulses():
	var indices_to_clear = []
	for i in range(len(impulse_array)):
		if impulse_time_array[i].x <= 0:
			indices_to_clear.append(i)
	indices_to_clear.reverse()
	for i in indices_to_clear:
		impulse_array.pop_at(i)
		impulse_time_array.pop_at(i)

func test_physics_material_collision():
	pass

func register_all_combo_actions():
	var registered_actions = [[]]
	var new_press_flag = false
	var previous_combo_state = combos.duplicate()
	for i in combo_actions:
		for j in range(len(i)):
			if Input.is_action_pressed(i[j]):
				registered_actions.append(i[j])
			if Input.is_action_just_pressed(i[j]):
				new_press_flag = true
	for i in registered_actions:
		registered_actions.sort()
	if registered_actions != [[]] and new_press_flag:
		for i in combos:
			if i.size() == 0:
				i.append(registered_actions)
				return
			if is_subset(i[-1], registered_actions): #Registered actions is subset fo combos
				i[-1] = registered_actions
			else:
				i.append(registered_actions)
			if i != previous_combo_state:
				check_for_combo()

func check_for_combo():
	if combos == [] or combos.size() == 0 or combos == [[]] or combos == [[[]]]:
		return
	for i in possible_combos:
		for j in range(len(i)):
			if i[j].actions == combos.slice(-i[j].actions.size(), -1):
				if self.has_method(i[j].function_name):
					call(i[j].function_name)

#func are_combos_equal_lenient(a, b):
#	var counter = 0
#	for i in range(a):
#		if not is_subset(i, b[counter]):
#			return false
#		counter += 1
#	return true
#
#func are_combos_equal(a, b, lenient: bool):
#	if lenient:
#		return are_combos_equal_lenient(a, b)
#	else:
#		return a == b

func set_animation_motion_parameter(motion: Vector2):
	pass
	#animation_tree.set('parameters/Idle/blend_position', motion)
	#animation_tree.set('parameters/Walking/blend_position', motion)

func reset_all_modifiers():
	speed_modifier = 1.0
	friction_modifier = 1.0
	time_scale_modifier = 1.0

func set_speed_modifier(x : float):
	speed_modifier = x

func set_friction_modifier(x : float):
	friction_modifier = x

func set_time_scale_modifier(x : float):
	time_scale_modifier = x

func sort_sub_arrays(x):
	for i in x:
		i.sort()

func get_kinematic_body_position():
	match dimensions:
		2:
			return kinematic_body.position
		3:
			return kinematic_body.position
		_:
			return

#Returns 0 if a is 0, 1 if a is positive, and -1 if a is negative.
func signed_value(a):
	if a == 0:
		return 0;
	else:
		return a / abs(a)

func is_subset(a, b): #Returns true if a is a subset of b, false otherwise
	for i in a:
		match typeof(b):
			TYPE_ARRAY:
				if b.find(i) < 0:
					return false
			TYPE_DICTIONARY:
				if !b.has(i):
					return false
			_:
				return false
	return true

func n_dimensional_zeros():
	match dimensions:
		2:
			return Vector2(0, 0)
		3:
			return Vector3(0, 0, 0)
		_:
			var n = []
			for i in range(dimensions):
				n.append(0)
			return n

func is_all_zeros(x):
	match typeof(x):
		TYPE_VECTOR2:
			return x == Vector2(0, 0)
		TYPE_VECTOR3:
			return x == Vector3(0, 0, 0)
#		TYPE_ARRAY:
#			for i in x:
#				if i != 0:
#					return false
#			return true
		_:
			return false

func get_dimensions():
	var x = dimensions
	return x

