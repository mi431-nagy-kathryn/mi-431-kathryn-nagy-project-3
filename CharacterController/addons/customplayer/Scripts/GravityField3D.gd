extends Area3D

@export var gravity_point_path: NodePath
@export var gravity_direction = Vector3(0, 1, 0)
@export var use_point_gravity: bool
@export var invert_point_gravity: bool
@onready var specified_gravity_point = get_node(gravity_point_path)

# Called when the node enters the scene tree for the first time.
func _ready():
	monitoring = true
	if not self.is_connected("area_entered", Callable(self, "on_area_enter")):
		self.connect("area_entered", Callable(self, "on_area_enter"))

func update_gravity(x : Object):
	if x.has_method("update_gravity_parameters"):
		x.update_gravity_parameters(use_point_gravity, invert_point_gravity, specified_gravity_point, gravity_direction)

func _on_Area_body_entered(body):
	update_gravity(body)
	update_gravity(body.get_parent())

func reset_gravity(x : Object):
	if x.has_method("reset_gravity_parameters"):
		x.reset_gravity_parameters()
