extends Resource
class_name GravitySettings

@export_category("Gravity and Jumping")
#Gravity Parameters
@export_group("Gravity Settings")
@export var gravity_direction_default = [Vector2(0, -1)] ##A vector representing the direction of gravity. Can be a Vector2 or Vector3. Arrays to represent n-dimensional vectors are theoretically possible.
@export var terminal_velocity = 3000.0 ##Terminal velocity in pixels/second (2D) or meters/second (3D)
@export var gravity_magnitude = 30.0 ##The magnitude of gravity, in pixels/second^2 or meters/second^2 (3D)
@export_subgroup("Gravity Falloff")
@export var enable_gravity_falloff: bool = false ##If true, gravity will get weaker as distance from the gravitational point specified in gravity_point_path_increases
@export var gravity_falloff: Curve
@export var gravity_falloff_distance: float = 1000
#Point Gravity - Attraction to a point instead of falling in a constant direction
@export_subgroup("Point Gravity")
@export var use_point_gravity_default = false ##If true, gravity will pull towards a specified point instead of being in a constant direction - great for a round planet or the like.
@export var point_gravity_repel_default = false ##If true, the point will repel instead of attract.
#@export var gravity_point_path: NodePath ##The path of the node that is used for gravity if point gravity is being used.
@export var align_with_gravity = true ##If true, the kinematic body will be rotated to align with the gravity direction.
@export var rotate_input_with_gravity = true ##If true, the input will be relative to the gravity direction, instead of absolute.
@export var gravity_rotation_offset = -1.5708 ##The rotation amount in radians by which the kinematic body is rotated compared to the gravity direction.

#Jump Parameters
@export_group("Jump Settings")
@export var jump_action_name: String = "jump" ##This is the name of the input action used for jumping. See Project Settings > Input Map.
@export_subgroup("Jump Trajectory") ##These modify the actual properties of the trajectory of the jump - height, and details of the curve.
@export var jump_height = 100.0 ##The height of the jump in pixels (2D) or meters (3D)
#@export var jump_strength = 750.0 ##The strength of the jump in pixels (2D) or meters (3D)
@export_range(0, 1) var jump_peak_timing = 0.5 ##The point in the jump when the player switches from rising to falling.
@export var falling_gravity_multiplier = 1.8 ##The factor by which gravity is stronger when falling compared to going up. 1.0 means both are equal which is realistic, many games use greater values for a weightier feel. If you wanted a floaty feel, go for a value between 1.0 and 0.0
@export var jump_height_range = 2.0 ##The factor by which the minimum jump height is smaller than the maximum jump height.
@export var jump_horizontal_velocity: float = 1.5 ##Applies horizontal velocity to the jump, as well as vertical. 1 is the same as ground movement, greater is a boost, less means a shorter jump.
@export_subgroup("When The Player Can Jump")
@export var max_jumps = 2 ##The maximum number of times the player can jump from the ground. This will be ignored if jump_limit is false.
@export var jump_limit = true ##If true, use max_jumps to determine how many times the player can jump without touching the ground in between. If false, jumps are unlimited.
@export var coyote_time = 0.2 ##The window of time in seconds that the player can still jump within after leaving the ground
@export var jump_buffer_time = 0.2 ##The window of time in seconds that a player's jump will be registered if it is within this window before hitting the ground again, and the player has used up all jumps.
@export var can_edge_jump = false ##If true, the player can jump from midair after falling, regardless of the time that has passed. There will still be a limited number of times per the previous variables.
@export_subgroup("Jump Combos and Boosting")
@export var landing_window_time = 0.5 ##The window of time in seconds after landing that special combinations can be executed within
@export var loop_chained_jumps: bool = true ##Determines behavior once the player has reached the maximum number of chained jumps. If true, it resets to 0, if false, it stays at its current value.
@export var jump_precision = [1.0, 0.2] ##Each value represents how precisely a player must time each jump, if multi-jumping is enabled. The index represents how many jumps since the last time touching the ground the player had before doing this jump. # (Array, float)
@export var jump_chain_boosts = [1.0, 1.0, 1.35] ##Each value represents the amount by which jump height is multiplied, based on how many jumps are chained together within the landing time frame after landing. # (Array, float)
@export var is_chain_boost_horizontal: bool = true
@export var is_chain_boost_vertical: bool = true
#Wall Jumping
@export_subgroup("Wall Jumping")
@export var wall_jumping_enabled: bool = true ##If true, the player can jump from walls.
@export var wall_friction: float = 0.9
@export var wall_bounce: float = 1.5 ##The amount by which the player bounces away from the wall when wall jumping.
