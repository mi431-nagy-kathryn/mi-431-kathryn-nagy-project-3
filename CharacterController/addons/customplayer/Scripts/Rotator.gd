extends Node
class_name Rotator

#Variables for rotation
@export_category("Manual Rotation")
@export_group("Node Paths")
@export var kinematic_body_path: NodePath
@export var rotation_target_path: NodePath
@export var controller_path: NodePath
@export_group("Rotation Input")
enum InputMode {SINGLE_INPUT, DUAL_INPUT}
@export var input_mode: InputMode
#Positive always rotates in one direction, negative always rotates in opposite.
@export var positive_rotation_action: String
@export var negative_rotation_action: String
#Alternative - there are two axes, and rotation target is specified as an angle to the specified value in the plane.
@export var multi_axis_rotation_actions = [[], []] # First index is x or y axis, second index is negative of positive input # (Array, Array, String)
enum RotationMode {LOOK_TO_POINT, MATCH_POINT_ORIENTATION}
@onready var rotation_target = get_node(rotation_target_path)
@onready var kinematic_body = get_node(kinematic_body_path)
@onready var controller = get_node(controller_path)
#export var is_movement_rotated = false
@export var rotation_mode: RotationMode = RotationMode.MATCH_POINT_ORIENTATION
@export_group("Rotation Settings")
@export var rotation_speed: float = 10.0
@export var rotation_interpolation: Curve
@export var rotation_interpolation_time = 1.0
@export var rotation_deceleration_time = 0.5
@export var use_local_target_position: bool = true
@export_subgroup("3D Rotation Only")
@export var rotation_axis: Vector3 = Vector3(0, 1, 0) #Only used in 3D calculations - the axis about which rotation occurs when applying the rotation offset.
@export var up_axis: Vector3 = Vector3(0, 1, 0) #Only used in 3D calculations
@export_subgroup("Hidden")
@export var rotation_offset = 1.5708

var current_rotation_target
var prev_rotation_target
var dimensions
var rotation_single_input = 0.0
var single_input_most_recent_direction = 0.0
var rotation_dual_input = Vector2(0.0, 0.0)
var rotation_interpolation_value = 0.0
var is_accelerating = true #True if accelerating, false if decelerating

func load_rotator_settings(settings: RotatorSettings):
	if (settings.input_mode == InputMode.SINGLE_INPUT):
		input_mode = InputMode.SINGLE_INPUT
	if (settings.input_mode == InputMode.DUAL_INPUT):
		input_mode = InputMode.DUAL_INPUT
	
	positive_rotation_action = settings.positive_rotation_action
	negative_rotation_action = settings.negative_rotation_action
	multi_axis_rotation_actions = settings.multi_axis_rotation_actions
	
	#rotation_mode = settings.rotation_mode
	if (settings.rotation_mode == RotationMode.LOOK_TO_POINT):
		rotation_mode = RotationMode.LOOK_TO_POINT
	if (settings.rotation_mode == RotationMode.MATCH_POINT_ORIENTATION):
		rotation_mode = RotationMode.MATCH_POINT_ORIENTATION
	
	rotation_speed = settings.rotation_speed
	rotation_interpolation = settings.rotation_interpolation
	rotation_interpolation_time = settings.rotation_interpolation_time
	rotation_deceleration_time = settings.rotation_deceleration_time
	rotation_offset = settings.rotation_offset
	use_local_target_position = settings.use_local_target_position
	
	rotation_axis = settings.rotation_axis
	up_axis = settings.rotation_axis
	

# Called when the node enters the scene tree for the first time.
func _ready():
	if !kinematic_body.has_method("move_and_slide"):
		print("Error: The kinematic body specified is not a valid kinematic body.")
	if !controller.has_method("calculate_base_velocity"):
		print("Error: The node specified for the controller is not a KinematicController.")
	dimensions = controller.get_dimensions()
	match dimensions:
		2:
			current_rotation_target = rotation_offset
			kinematic_body.rotation = rotation_offset
		3:
			current_rotation_target = Basis(rotation_axis, rotation_offset)
			kinematic_body.transform.basis = current_rotation_target
		_:
			print("Error: No dimensions value specified. If using a custom dimension number, please add support for it. The value is: ", dimensions)
	prev_rotation_target = current_rotation_target
	if rotation_interpolation_time == 0.0:
		rotation_interpolation_time = 0.01
	if rotation_deceleration_time == 0.0:
		rotation_deceleration_time = 0.01

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	#set_axis_inputs_state()
	var base_velocity = controller.calculate_base_velocity(delta)
	var zeros = controller.n_dimensional_zeros()
	var time_scale = controller.time_scale_modifier
	if time_scale == null:
		time_scale = 1.0
	rotation_interpolation_value = clamp(rotation_interpolation_value + ((delta * time_scale) / rotation_interpolation_time) if is_accelerating else rotation_interpolation_value - ((delta * time_scale) / rotation_deceleration_time), 0.0, 1.0)
	
	#TEMPORARY - NOT COMPATIBLE WITH MULTIPLAYER OR NPC
	
	
	
#	if is_movement_rotated:
#		kinematic_body.move_and_slide(base_velocity.rotated(kinematic_body.rotation))
#	else:
#		kinematic_body.move_and_slide(base_velocity)
	
	match rotation_mode:
		RotationMode.LOOK_TO_POINT:
			update_rotation_target_parent(delta * time_scale)
			if dimensions == 2:
#				if use_local_target_position:
#					rotation_target.get_parent().global_position = kinematic_body.global_position
				kinematic_body.look_at(rotation_target.position)
				kinematic_body.rotate(rotation_offset)
			elif dimensions == 3:
				if use_local_target_position:
					rotation_target.get_parent().global_transform.origin = kinematic_body.global_transform.origin
				kinematic_body.look_at(rotation_target.global_transform.origin, Vector3.UP)
				kinematic_body.rotate(rotation_axis.normalized(), rotation_offset)
		RotationMode.MATCH_POINT_ORIENTATION:
			update_rotation_target(delta * time_scale)
			if dimensions == 2:
				kinematic_body.global_rotation = rotation_target.global_rotation + rotation_offset
			if dimensions == 3:
				kinematic_body.transform.basis = rotation_target.transform.basis
				kinematic_body.rotate(rotation_axis.normalized(), rotation_offset)
#		RotationMode.MATCH_POINT_ORIENTATION_WITH_INTERPOLATION:
#			update_rotation_target(delta * time_scale)
#			if dimensions == 2:
#				kinematic_body.global_rotation = rotation_target.global_rotation + rotation_offset
#			if dimensions == 3:
#				kinematic_body.transform.basis = rotation_target.transform.basis
#				kinematic_body.rotate(rotation_axis.normalized(), rotation_offset)
#		RotationMode.ALIGN_TO_DIRECTION:
#			var velocity_temp = base_velocity
#			print(rotation_interpolation_value)
#			#if (current_rotation_target != prev_rotation_target):
#			#	rotation_interpolation_value = (delta * time_scale) / rotation_interpolation_time
#			if velocity_temp != zeros:
#				if dimensions == 2:
#					current_rotation_target = velocity_temp.angle()
#					#kinematic_body.global_rotation = lerp_angle(kinematic_body.global_rotation, current_rotation_target + rotation_offset, rotation_interpolation.interpolate(rotation_interpolation_value)) 
#				if dimensions == 3:
#					#current_rotation_target = velocity_temp.angle_to(up_axis) #Change this?
#					var rotation_amount = kinematic_body.global_transform.basis.z.angle_to(velocity_temp)
#					var temp_rotation_axis = kinematic_body.global_transform.basis.z.cross(velocity_temp)
#					current_rotation_target = Basis(temp_rotation_axis, rotation_amount).orthonormalized()
#			if dimensions == 2:
#				kinematic_body.global_rotation = lerp_angle(kinematic_body.global_rotation, current_rotation_target + rotation_offset, rotation_interpolation.interpolate(rotation_interpolation_value)) 
#			if dimensions == 3:
#				kinematic_body.global_transform.basis = kinematic_body.global_transform.basis.slerp(current_rotation_target * Basis(rotation_axis, rotation_offset), rotation_interpolation.interpolate(rotation_interpolation_value)).orthonormalized()
	prev_rotation_target = current_rotation_target

#TEMPORARY - NOT COMPATIBLE WITH MULTIPLAYER OR NPC
func _input(event):
	pass

#Updates the axis_inputs based on a specific event.
func set_inputs_event(event: InputEvent):
	match input_mode:
		InputMode.SINGLE_INPUT:
			if event.is_action_just_pressed(negative_rotation_action):
				rotation_single_input = -1
				reset_interpolation_value()
				return
			if event.is_action_just_pressed(positive_rotation_action):
				rotation_single_input = 1
				reset_interpolation_value()
				return
			if event.is_action_released(negative_rotation_action):
				rotation_single_input = 0
			if event.is_action_released(positive_rotation_action):
				rotation_single_input = 0
			if event.is_action_pressed(negative_rotation_action):
				rotation_single_input += -1
			if event.is_action_pressed(positive_rotation_action):
				rotation_single_input += 1
			if single_input_most_recent_direction == 0 or single_input_most_recent_direction == -rotation_single_input:
				single_input_most_recent_direction = rotation_single_input
		InputMode.DUAL_INPUT:
			for i in range(len(rotation_dual_input)):
				if event.is_action_just_pressed(multi_axis_rotation_actions[i][0]):
					rotation_dual_input[i] = -1
					reset_interpolation_value()
					return
				if event.is_action_just_pressed(multi_axis_rotation_actions[i][1]):
					rotation_dual_input[i] = 1
					reset_interpolation_value()
					return
				if event.is_action_released(multi_axis_rotation_actions[i][0]):
					rotation_dual_input[i] = 0
				if event.is_action_released(multi_axis_rotation_actions[i][1]):
					rotation_dual_input[i] = 0
				if event.is_action_pressed(multi_axis_rotation_actions[i][0]):
					rotation_dual_input[i] += -1
				if event.is_action_pressed(multi_axis_rotation_actions[i][1]):
					rotation_dual_input[i] += 1
	reset_interpolation_value()

#Updates the axis_inputs based on the overall input state.
func set_inputs_state():
	#if rotation_mode == RotationMode.ALIGN_TO_DIRECTION:
	#	return
	match input_mode:
		InputMode.SINGLE_INPUT:
			rotation_single_input = 0
			if Input.is_action_just_pressed(negative_rotation_action):
				rotation_single_input = -1
				reset_interpolation_value()
				return
			if Input.is_action_just_pressed(positive_rotation_action):
				rotation_single_input = 1
				reset_interpolation_value()
				return
			if Input.is_action_pressed(negative_rotation_action):
				rotation_single_input += -1
			if Input.is_action_pressed(positive_rotation_action):
				rotation_single_input += 1
			if single_input_most_recent_direction == 0 or single_input_most_recent_direction == -rotation_single_input:
				single_input_most_recent_direction = rotation_single_input
		InputMode.DUAL_INPUT:
			for i in range(2):
				if Input.is_action_just_pressed(multi_axis_rotation_actions[i][0]):
					rotation_dual_input[i] = -1
					reset_interpolation_value()
					return
				if Input.is_action_just_pressed(multi_axis_rotation_actions[i][1]):
					rotation_dual_input[i] = 1
					reset_interpolation_value()
					return
				if Input.is_action_pressed(multi_axis_rotation_actions[i][0]):
					rotation_dual_input[i] += -1
				if Input.is_action_pressed(multi_axis_rotation_actions[i][1]):
					rotation_dual_input[i] += 1
	reset_interpolation_value()

#Updates the axis_inputs based on an array with specified values (such as for use with an NPC)
func set_inputs_raw(x):
	match input_mode:
		InputMode.SINGLE_INPUT:
			rotation_single_input = x
			if single_input_most_recent_direction == 0 or single_input_most_recent_direction == -rotation_single_input:
				single_input_most_recent_direction = rotation_single_input
		InputMode.DUAL_INPUT:
			rotation_dual_input = x
	reset_interpolation_value()

func reset_interpolation_value():
	match input_mode:
		InputMode.SINGLE_INPUT:
			if rotation_single_input == 0:
				is_accelerating = false
			else:
				is_accelerating = true
		InputMode.DUAL_INPUT:
			if rotation_dual_input == Vector2(0, 0):
				is_accelerating = false
			else:
				is_accelerating = true

func update_rotation_target_parent(delta):
	#reset_interpolation_value()
	match input_mode:
		InputMode.SINGLE_INPUT:
			if dimensions == 2:
				rotation_target.get_parent().global_rotation += rotation_speed * single_input_most_recent_direction * delta * rotation_interpolation.sample(rotation_interpolation_value)
				#if (rotation_single_input != 0):
					#print(rotation_target.get_parent().global_rotation)
			if dimensions == 3:
				rotation_target.get_parent().rotate(rotation_axis, rotation_speed * single_input_most_recent_direction * delta * rotation_interpolation.sample(rotation_interpolation_value))
#				if (rotation_single_input != 0):
#					print(rotation_target.get_parent().global_transform)
		InputMode.DUAL_INPUT:
			if dimensions == 2:
				rotation_target.get_parent().global_rotation = rotation_dual_input.angle()
			if dimensions == 3:
				if rotation_dual_input != Vector2(0, 0):
					rotation_target.get_parent.global_transform.basis = Basis(rotation_axis, rotation_dual_input.angle())
				#pass # rotation_dual_input.angle()

func update_rotation_target(delta):
	#print("Updating rotation target")
	#reset_interpolation_value()
	match input_mode:
		InputMode.SINGLE_INPUT:
			if dimensions == 2:
				rotation_target.global_rotation += rotation_speed * single_input_most_recent_direction * delta * rotation_interpolation.sample(rotation_interpolation_value)
				#if (rotation_single_input != 0):
					#print(rotation_target.get_parent().global_rotation)
			if dimensions == 3:
				#print(rotation_target.global_transform, " before")
				rotation_target.rotate(rotation_axis, rotation_speed * single_input_most_recent_direction * delta * rotation_interpolation.sample(rotation_interpolation_value))
				#print(rotation_target.global_transform, " after")
#				if (rotation_single_input != 0):
#					print(rotation_target.get_parent().global_transform)
		InputMode.DUAL_INPUT:
			if dimensions == 2:
				rotation_target.global_rotation = rotation_dual_input.angle()
				#print(rotation_dual_input.angle())
				#kinematic_body.global_rotation = lerp_angle(kinematic_body.global_rotation, rotation_dual_input.angle() + rotation_offset, rotation_interpolation.interpolate(rotation_interpolation_value)) 
			if dimensions == 3:
				if rotation_dual_input != Vector2(0, 0):
					rotation_target.get_parent.global_transform.basis = Basis(rotation_axis, rotation_dual_input.angle())
				#pass # rotation_dual_input.angle()
