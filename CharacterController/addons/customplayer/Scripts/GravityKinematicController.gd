extends KinematicController

@export_category("Gravity and Jumping")
#Gravity Parameters
@export_group("Gravity Settings")
@export var gravity_direction_default = [Vector2(0, -1)] ##A vector representing the direction of gravity. Can be a Vector2 or Vector3. Arrays to represent n-dimensional vectors are theoretically possible.
@export var terminal_velocity = 3000.0 ##Terminal velocity in pixels/second (2D) or meters/second (3D)
@export var gravity_magnitude = 30.0 ##The magnitude of gravity, in pixels/second^2 or meters/second^2 (3D)
@export_subgroup("Gravity Falloff")
@export var enable_gravity_falloff: bool = false ##If true, gravity will get weaker as distance from the gravitational point specified in gravity_point_path_increases
@export var gravity_falloff: Curve
@export var gravity_falloff_distance: float = 1000
#Point Gravity - Attraction to a point instead of falling in a constant direction
@export_subgroup("Point Gravity")
@export var use_point_gravity_default = false ##If true, gravity will pull towards a specified point instead of being in a constant direction - great for a round planet or the like.
@export var point_gravity_repel_default = false ##If true, the point will repel instead of attract.
@export var gravity_point_path: NodePath ##The path of the node that is used for gravity if point gravity is being used.
@export var align_with_gravity = true ##If true, the kinematic body will be rotated to align with the gravity direction.
@export var rotate_input_with_gravity = true ##If true, the input will be relative to the gravity direction, instead of absolute.
@export_subgroup("Hidden")
@export var gravity_rotation_offset = -1.5708 ##The rotation amount in radians by which the kinematic body is rotated compared to the gravity direction.

#Jump Parameters
@export_group("Jump Settings")
@export var jump_action_name: String = "jump" ##This is the name of the input action used for jumping. See Project Settings > Input Map.
@export_subgroup("Jump Trajectory") ##These modify the actual properties of the trajectory of the jump - height, and details of the curve.
@export var jump_height = 100.0 ##The height of the jump in pixels (2D) or meters (3D)
#@export var jump_strength = 750.0 ##The strength of the jump in pixels (2D) or meters (3D)
@export_range(0, 1) var jump_peak_timing = 0.5 ##The point in the jump when the player switches from rising to falling.
@export var falling_gravity_multiplier = 1.8 ##The factor by which gravity is stronger when falling compared to going up. 1.0 means both are equal which is realistic, many games use greater values for a weightier feel. If you wanted a floaty feel, go for a value between 1.0 and 0.0
@export var jump_height_range = 2.0 ##The factor by which the minimum jump height is smaller than the maximum jump height.
@export var jump_horizontal_velocity: float = 1.5 ##Applies horizontal velocity to the jump, as well as vertical. 1 is the same as ground movement, greater is a boost, less means a shorter jump.
@export_subgroup("When The Player Can Jump")
@export var max_jumps = 2 ##The maximum number of times the player can jump from the ground. This will be ignored if jump_limit is false.
@export var jump_limit = true ##If true, use max_jumps to determine how many times the player can jump without touching the ground in between. If false, jumps are unlimited.
@export var coyote_time = 0.2 ##The window of time in seconds that the player can still jump within after leaving the ground
@export var jump_buffer_time = 0.2 ##The window of time in seconds that a player's jump will be registered if it is within this window before hitting the ground again, and the player has used up all jumps.
@export var can_edge_jump = false ##If true, the player can jump from midair after falling, regardless of the time that has passed. There will still be a limited number of times per the previous variables.
@export_subgroup("Jump Combos and Boosting")
@export var landing_window_time = 0.5 ##The window of time in seconds after landing that special combinations can be executed within
@export var loop_chained_jumps: bool = true ##Determines behavior once the player has reached the maximum number of chained jumps. If true, it resets to 0, if false, it stays at its current value.
@export var jump_precision = [1.0, 0.2] ##Each value represents how precisely a player must time each jump, if multi-jumping is enabled. The index represents how many jumps since the last time touching the ground the player had before doing this jump. # (Array, float)
@export var jump_chain_boosts = [1.0, 1.0, 1.35] ##Each value represents the amount by which jump height is multiplied, based on how many jumps are chained together within the landing time frame after landing. # (Array, float)
@export var is_chain_boost_horizontal: bool = true
@export var is_chain_boost_vertical: bool = true
#Wall Jumping
@export_subgroup("Wall Jumping")
@export var wall_jumping_enabled: bool = true ##If true, the player can jump from walls.
@export var wall_friction: float = 0.9
@export var wall_bounce: float = 1.5 ##The amount by which the player bounces away from the wall when wall jumping.


@onready var use_point_gravity = use_point_gravity_default
@onready var point_gravity_repel = point_gravity_repel_default
@onready var gravity_direction = gravity_direction_default

var gravitational_velocity = 0.0
var jumps = 0
var chained_jumps = 0
var coyote_timer
var jump_buffer_timer
var landing_window_timer
var jumping = false

@onready var gravity_point_default = get_node(gravity_point_path)
@onready var gravity_point = gravity_point_default

var jump_input_pressed : bool = false
var jump_input_just_pressed: bool = false
var jump_input_just_released: bool = false

var jump_index

var previous_frame_gravitational_velocity = 0.0
var bounces = 0

var fall_speed_modifier = 1.0 #Lower values will extend fall time, since they will decrease fall speed
var jump_height_modifier = 1.0 #Higher values will increase the jump height
var jump_horizontal_modifier = 1.0

func load_gravity_settings(settings: GravitySettings):
	gravity_direction_default = settings.gravity_direction_default
	terminal_velocity = settings.terminal_velocity
	gravity_magnitude = settings.gravity_magnitude
	
	enable_gravity_falloff = settings.enable_gravity_falloff
	gravity_falloff = settings.gravity_falloff
	gravity_falloff_distance = settings.gravity_falloff_distance
	
	use_point_gravity_default = settings.use_point_gravity_default
	point_gravity_repel_default = settings.point_gravity_repel_default
	align_with_gravity = settings.align_with_gravity
	rotate_input_with_gravity = settings.rotate_input_with_gravity
	gravity_rotation_offset = settings.gravity_rotation_offset
	
	jump_action_name = settings.jump_action_name
	jump_height = settings.jump_height
	jump_peak_timing = settings.jump_peak_timing
	falling_gravity_multiplier = settings.falling_gravity_multiplier
	jump_height_range = settings.jump_height_range
	jump_horizontal_velocity = settings.jump_horizontal_velocity
	
	max_jumps = settings.max_jumps
	jump_limit = settings.jump_limit
	coyote_time = settings.coyote_time
	jump_buffer_time = settings.jump_buffer_time
	can_edge_jump = settings.can_edge_jump
	
	landing_window_time = settings.landing_window_time
	loop_chained_jumps = settings.loop_chained_jumps
	jump_precision = settings.jump_precision
	jump_chain_boosts = settings.jump_chain_boosts
	is_chain_boost_horizontal = settings.is_chain_boost_horizontal
	is_chain_boost_vertical = settings.is_chain_boost_vertical
	
	wall_jumping_enabled = settings.wall_jumping_enabled
	wall_friction = settings.wall_friction
	wall_bounce = settings.wall_bounce

# Called when the node enters the scene tree for the first time.
func _ready():
	print("Starting ready in GravityKinematicController")
	
	if dimensions == null or dimensions == 0:
		initialize_motion_vectors()
		initialize_actions()
		initialize_physics()
	
	if kinematic_body == null:
		print("Null kinematic body")
		
	var gravity_dimensions
	match(typeof(gravity_direction[0])):
		TYPE_VECTOR2:
			gravity_dimensions = 2
		TYPE_VECTOR3:
			gravity_dimensions = 3
		TYPE_ARRAY:
			gravity_dimensions = base_velocity.size()
	if gravity_dimensions != dimensions:
		print("Error: The gravity vector and motion vectors are different sizes. The gravity vector is ", gravity_dimensions, "-dimensional and the motion vectors are ", dimensions, "-dimensional.")
	if not kinematic_body.has_method("move_and_slide"):
		print("Error: The node loaded from the kinematic_body_path variable is not a kinematic body.")
	
	coyote_timer = coyote_time
	jump_buffer_timer = jump_buffer_time
	landing_window_timer = landing_window_time
	
	call_upon_ready()

func call_upon_ready():
	jump_index = input_action_dictionary[jump_action_name]


func _physics_process(delta):
	
	var scaled_delta = delta * time_scale_modifier
	
	#set_axis_inputs_state() #This is only for testing, not for NPCs
	
	test_physics_material_collision()
	
	if use_point_gravity:
		gravity_direction[0] = gravity_point.global_position - kinematic_body.global_position
		gravity_direction[0] = gravity_direction[0].normalized()
		if point_gravity_repel:
			gravity_direction[0] *= -1
	if align_with_gravity:
		if typeof(gravity_direction[0]) == TYPE_VECTOR2:
			kinematic_body.rotation = gravity_direction[0].angle() + gravity_rotation_offset
	
	var bounce_flag = false
	var bounce_direction
	if kinematic_body.get_last_slide_collision():
		if kinematic_body.get_last_slide_collision().get_collider():
			if previous_frame_gravitational_velocity > 0:
				bounce_flag = true
				last_collision_normal = kinematic_body.get_last_slide_collision().get_normal()
				if jumping:
					landing_window_timer = 0.0
	
	if (not jumping) and (not can_edge_jump):
		coyote_timer += scaled_delta
	jump_buffer_timer += scaled_delta
	landing_window_timer += scaled_delta
	
	var total_velocity = calculate_base_velocity(delta)
	var gravity_factor = 1.0
	var gravity_distance_factor = 1.0
	var bounce_velocity
	if enable_gravity_falloff:
		if use_point_gravity:
			gravity_distance_factor = gravity_falloff.sample(abs(get_kinematic_body_position().distance_to(get_gravity_point_position()) / gravity_falloff_distance))
		else:
			#print((kinematic_body.position * gravity_direction).distance_to(gravity_point.position * gravity_direction))
			gravity_distance_factor = gravity_falloff.sample(abs((get_kinematic_body_position() * gravity_direction[0]).distance_to(get_gravity_point_position() * gravity_direction[0]) / gravity_falloff_distance))
		print(gravity_distance_factor)
	if rotate_input_with_gravity:
		if dimensions == 2:
			kinematic_body.look_at(get_gravity_point_position())
			kinematic_body.rotate(gravity_rotation_offset)
			total_velocity = total_velocity.rotated(kinematic_body.global_rotation)
		if dimensions == 3:
			kinematic_body.look_at(get_gravity_point_position(), -gravity_direction[0])
	
	if input_actions[jump_index][1]:
		jump_buffer_timer = 0
	elif !input_actions[jump_index][0]:
		gravity_factor = jump_height_range
	while len(jump_precision) <= jumps:
		jump_precision.append(1.0)
	if jump_buffer_timer < jump_buffer_time and coyote_timer < coyote_time and (jumps < max_jumps or not jump_limit) and (abs(gravitational_velocity) < sqrt(2 * gravity_magnitude * jump_height) * jump_precision[jumps]): #
		gravitational_velocity = -sqrt(2 * gravity_magnitude * jump_height) * jump_height_modifier
		jumps += 1
		jumping = true
		on_jump(scaled_delta)
		jump_buffer_timer = jump_buffer_time
		if kinematic_body.is_on_wall() and !kinematic_body.is_on_floor():
			apply_impulse(0.1, wall_bounce * kinematic_body.get_last_slide_collision().get_normal() * speed)
			#
		bounces = 0
		
		
		if landing_window_timer < landing_window_time:
			chained_jumps += 1
		else:
			chained_jumps = 0
		
		if chained_jumps >= len(jump_chain_boosts):
			if loop_chained_jumps:
				chained_jumps = 0
			else:
				chained_jumps = len(jump_chain_boosts) - 1
		
		gravitational_velocity *= (sqrt(jump_chain_boosts[chained_jumps]) if is_chain_boost_vertical else 1.0)
#	elif bounce_flag and bounces < max_bounces:
#		#gravitational_velocity = -previous_frame_gravitational_velocity * last_physics_material[0]
#		total_velocity = ((total_velocity) + (previous_frame_gravitational_velocity * gravity_direction)).length() * bounce_direction * last_physics_material[0]
#		print(total_velocity)
#		bounces += 1
	elif kinematic_body.is_on_wall():
		if wall_jumping_enabled:
			jumps = 0
			coyote_timer = 0.0
			#jumping = false
		gravitational_velocity += gravity_magnitude * gravity_distance_factor * (1.0 - wall_friction)
	elif kinematic_body.is_on_floor():
		#apply_impulse(jump_strength * -gravity_direction, 1.0)
		gravitational_velocity = 0.0
		jumps = 0
		coyote_timer = 0.0
		jumping = false
	else: # Gravity is being applied
		if gravitational_velocity > 0:
			gravity_factor = falling_gravity_multiplier * fall_speed_modifier
			#animation_node.travel("Falling")
		gravitational_velocity += gravity_magnitude * gravity_factor * gravity_distance_factor * time_scale_modifier
		gravitational_velocity = min(gravitational_velocity, terminal_velocity)
	if jumping:
		total_velocity *= jump_horizontal_velocity * jump_horizontal_modifier * (jump_chain_boosts[chained_jumps] if is_chain_boost_horizontal else 1.0)
		if gravitational_velocity < 0:
			pass
			#animation_node.travel("Jumping")
	if kinematic_body.is_on_ceiling():
		gravitational_velocity = max(gravitational_velocity, 0)
	
	total_velocity += gravitational_velocity * gravity_direction[0]
#	if bounce_velocity != null:
#		print(total_velocity, total_velocity + bounce_velocity)
#		total_velocity += bounce_velocity
	
	previous_frame_gravitational_velocity = gravitational_velocity
	
	if bounce_flag and (previous_frame_velocity * last_collision_normal).length() > last_physics_material[1]:
#		print("Previous velocity: ", previous_frame_velocity, "  Bounce velocity: ", previous_frame_velocity.length() * bounce_direction)
		apply_bounce_impulse(last_physics_material[0])
		bounces += 1
	
	previous_frame_velocity = total_velocity
	kinematic_body.set_velocity(total_velocity * time_scale_modifier)
	kinematic_body.set_up_direction(-gravity_direction[0])
	kinematic_body.set_floor_stop_on_slope_enabled(stop_on_slope)
	kinematic_body.set_max_slides(max_slides)
	kinematic_body.set_floor_max_angle(floor_max_angle)
	# TODOConverter3To4 infinite_inertia were removed in Godot 4 - previous value `infinite_inertia`
	kinematic_body.move_and_slide()
	kinematic_body.velocity

func test_physics_material_collision():
	if kinematic_body == null:
		return
	var object = kinematic_body.get_last_slide_collision()
	if object == null or object.get_collider() == null:
		return
	if object.get_collider().has_method("physics_properties_in_array"):
		last_physics_material = object.get_collider().physics_properties_in_array()
	else:
		last_physics_material = [default_bounciness, default_bounce_threshold, default_static_friction, default_dynamic_friction]

#func set_jump_input_raw(pressed: bool):
#	jump_input_just_pressed = pressed and not jump_input_pressed
#	jump_input_just_released = not pressed and jump_input_pressed
#	jump_input_pressed = pressed
#
#func set_jump_input_state(action: String):
#	jump_input_pressed = Input.is_action_pressed(action)
#	jump_input_just_pressed = Input.is_action_just_pressed(action)
#	jump_input_just_released = Input.is_action_just_released(action)

func update_gravity_parameters(is_point : bool, is_repel : bool, point, constant_direction):
	use_point_gravity = is_point
	point_gravity_repel = is_repel
	gravity_direction[0] = constant_direction
	gravity_point = point

func reset_gravity_parameters():
	use_point_gravity = use_point_gravity_default
	point_gravity_repel = point_gravity_repel_default
	gravity_direction[0] = gravity_direction_default
	gravity_point = gravity_point_default

func get_gravity_point_position():
	if gravity_point == null:
		return null
	match dimensions:
		2:
			return gravity_point.position
		3:
			return gravity_point.position
		_:
			return

func reset_all_modifiers():
	speed_modifier = 1.0
	friction_modifier = 1.0
	time_scale_modifier = 1.0
	fall_speed_modifier = 1.0
	jump_height_modifier = 1.0
	jump_horizontal_modifier = 1.0

func set_fall_speed_modifier(x : float):
	fall_speed_modifier = x

func set_jump_height_modifier(x : float):
	jump_height_modifier = x

func set_jump_horizontal_modifier(x : float):
	jump_horizontal_modifier = x

func test_combo():
	print("Test combo successfully called")

func set_animation_motion_parameter(motion: Vector2):
	#animation_tree.set('parameters/Idle/blend_position', motion)
	#animation_tree.set('parameters/Walking/blend_position', motion)
	#animation_tree.set('parameters/Jumping/blend_position', motion)
	#animation_tree.set('parameters/Falling/blend_position', motion)
	pass

## Blank functions to be customized in inherited script
#  These are meant to code custom behavior.
func on_jump(delta):
	#jumps and chained_jumps are useful variables to consider for custom behavior.
	if landing_window_timer < landing_window_time:
		#This jump was executed shortly after touching the ground, within the window of time used for chaining jumps.
		pass
	if coyote_timer > delta * 2.0:
		#This jump was executed shortly after leaving the ground.
		pass
	if jump_buffer_timer > 0:
		#This jump was buffered - the jump key was pressed before touching the ground.
		pass
	return

func on_land():
	return
