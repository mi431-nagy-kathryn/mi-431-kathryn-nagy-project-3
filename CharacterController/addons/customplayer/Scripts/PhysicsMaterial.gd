extends Node

@export var bounciness: float = 0.0
@export var bounce_threshold: float = 500.0
@export var static_friction: float = 1.0
@export var dynamic_friction: float = 1.0

func physics_properties_in_array():
	return [bounciness, bounce_threshold, static_friction, dynamic_friction]

func get_bounciness():
	return bounciness

func get_static_friction():
	return static_friction

func get_dynamic_friction():
	return dynamic_friction
