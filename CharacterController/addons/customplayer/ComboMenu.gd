@tool
extends Node

@export var default_save_path = "res://Resources/Combos/"

var combo_to_save
var last_selected_item_list

# Called when the node enters the scene tree for the first time.
func _ready():
	$FileDialog.current_path = default_save_path
	last_selected_item_list = $ScrollContainer/VBoxContainer/ItemList


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			for child in $ScrollContainer/VBoxContainer.get_children():
				if child.has_signal("item_clicked"):
					if child.has_focus():
						last_selected_item_list = child

func _on_new_action_button_pressed():
	if ($ScrollContainer/VBoxContainer/NewActionContainer/NewActionLineEdit.text != ""):
		last_selected_item_list.add_item($ScrollContainer/VBoxContainer/NewActionContainer/NewActionLineEdit.text)
		$ScrollContainer/VBoxContainer/NewActionContainer/NewActionLineEdit.text = ""


func _on_new_action_group_button_pressed():
	var item_list = ItemList.new()
	item_list.auto_height = true
	$ScrollContainer/VBoxContainer.add_child(item_list)
	last_selected_item_list = item_list
	
func generate_combo_array():
	var return_array = []
	for child in $ScrollContainer/VBoxContainer.get_children():
		var sub_array = []
		if child.has_signal("item_clicked"):
			for i in range(0, child.item_count):
				sub_array.append(child.get_item_text(i))
			return_array.append(sub_array)
	return return_array


func _on_button_pressed():
	print(generate_combo_array())
	combo_to_save = Combo.new()
	combo_to_save.actions = generate_combo_array()
	combo_to_save.function_name = $ScrollContainer/VBoxContainer/FunctionNameContainer/FunctionNameLineEdit.text
	combo_to_save.use_strict_equality_actions = $ScrollContainer/VBoxContainer/StrictEqualityActionContainer/CheckBox.button_pressed
	combo_to_save.use_strict_equality_instant = $ScrollContainer/VBoxContainer/StrictEqualityInstantContainer/CheckBox.button_pressed
	$FileDialog.visible = true
	print($FileDialog.current_path)


func _on_file_dialog_confirmed():
	var file_path = $FileDialog.current_path
	if (file_path == ""):
		return
	if (!file_path.ends_with(".tres") && !file_path.ends_with(".res")):
		file_path += ".tres"
	var result = ResourceSaver.save(combo_to_save, file_path)
	assert(result == OK)
	#Clear the file dialog
	$FileDialog.current_file = ""
	for child in $ScrollContainer/VBoxContainer.get_children():
		if child.has_signal("item_clicked"):
			child.free()
	var item_list = ItemList.new()
	item_list.auto_height = true
	$ScrollContainer/VBoxContainer.add_child(item_list)
	last_selected_item_list = item_list



