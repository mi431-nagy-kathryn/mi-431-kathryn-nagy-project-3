@tool
extends Node

@export var playerToLoad: PackedScene
@export var PlayerTopDown: PackedScene
@export var PlayerSidescrolling: PackedScene
@export var Player3D: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_7_pressed():
	var playerInstance = playerToLoad.instantiate()
	get_tree().get_edited_scene_root().add_child(playerInstance)
	playerInstance.owner = get_tree().get_edited_scene_root()




func _on_top_down_button_pressed():
	var playerInstance = PlayerTopDown.instantiate()
	get_tree().get_edited_scene_root().add_child(playerInstance)
	playerInstance.owner = get_tree().get_edited_scene_root()


func _on_button_2_pressed():
	var playerInstance = PlayerSidescrolling.instantiate()
	get_tree().get_edited_scene_root().add_child(playerInstance)
	playerInstance.owner = get_tree().get_edited_scene_root()


func _on_button_5_pressed():
	var playerInstance = Player3D.instantiate()
	get_tree().get_edited_scene_root().add_child(playerInstance)
	playerInstance.owner = get_tree().get_edited_scene_root()
