@tool
extends EditorPlugin

var documentationInterface = preload("res://addons/customplayer/InterfaceScenes/MainInterface.tscn").instantiate()
var playerInstanceInterface = preload("res://addons/customplayer/InterfaceScenes/PlayerType.tscn")
var comboInterface = preload("res://addons/customplayer/InterfaceScenes/ComboMenu.tscn")

var playerInstanceInterfaceScene
var comboInterfaceScene

func _enter_tree():
	get_editor_interface().get_editor_main_screen().add_child(documentationInterface)
	playerInstanceInterfaceScene = playerInstanceInterface.instantiate()
	comboInterfaceScene = comboInterface.instantiate()
	add_control_to_dock(EditorPlugin.DOCK_SLOT_LEFT_UR, playerInstanceInterfaceScene)
	add_control_to_dock(EditorPlugin.DOCK_SLOT_LEFT_UR, comboInterfaceScene)
	_make_visible(false)

func _exit_tree():
	if documentationInterface:
		documentationInterface.queue_free()
	if playerInstanceInterfaceScene:
		remove_control_from_docks(playerInstanceInterfaceScene)
		playerInstanceInterfaceScene.free()
	if comboInterfaceScene:
		comboInterfaceScene.free()

func _has_main_screen():
	return true

func _get_plugin_name():
	return "Custom Player Controller"

#func _get_plugin_icon():
#	return get_editor_interface().get_base_control().get_icon("CharacterBody2D", "EditorIcons")

func _make_visible(visible):
	if documentationInterface:
		documentationInterface.visible = visible
