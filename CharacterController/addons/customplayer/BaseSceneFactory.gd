@tool
extends Control

enum Perspective {TOP_DOWN, SIDESCROLLING, THREE_DIMENSIONAL}
enum CameraType {FirstPerson, ThirdPerson}
enum DefaultGravity {ConstantDirection, PointAttract, PointRepel}
enum SceneOptions {None, MinimalEnvironment, FullEnvironment}

var current_perspective = null
var current_camera_type = null
var current_default_gravity = null
var current_scene_option = null
var attach_trail_renderer = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_perspective(index : int):
	match index:
		0:
			current_perspective = Perspective.TOP_DOWN
			$Label.text = "Current is Top-Down"
		1:
			current_perspective = Perspective.SIDESCROLLING
			$Label.text = "Current is Sidescrolling"
		2:
			current_perspective = Perspective.THREE_DIMENSIONAL
			$Label.text = "Current is 3D"
		_:
			return

func set_camera_type(index: int):
	match index:
		0:
			current_camera_type = CameraType.FirstPerson
		1:
			current_camera_type = CameraType.ThirdPerson
		_:
			return

func set_default_gravity(index: int):
	match index:
		0:
			current_default_gravity = DefaultGravity.ConstantDirection
		1:
			current_default_gravity = DefaultGravity.PointAttract
		2:
			current_default_gravity = DefaultGravity.PointRepel
		_:
			return

func set_scene_options(index: int):
	match index:
		0:
			current_scene_option = SceneOptions.None
		1:
			current_scene_option = SceneOptions.MinimalEnvironment
		2:
			current_scene_option = SceneOptions.FullEnvironment
		_:
			return

func set_selector_visibility():
	$CameraTypeSelector.visible = (current_perspective == Perspective.THREE_DIMENSIONAL)
	if ($CameraTypeSelector.visible and $CameraTypeSelector.get_selected_items().size() > 0):
		set_camera_type($CameraTypeSelector.get_selected_items()[0])
	else:
		current_camera_type = null
	
	$DefaultGravitySelector.visible = (current_perspective != Perspective.TOP_DOWN)
	if ($DefaultGravitySelector.visible and $DefaultGravitySelector.get_selected_items().size() > 0):
		set_default_gravity($DefaultGravitySelector.get_selected_items()[0])
	else:
		current_default_gravity = null
	
	return


func _on_PerspectiveSelector_item_selected(index):
	set_perspective(index)


func _on_TrailRendererCheckbox_toggled(button_pressed):
	attach_trail_renderer = button_pressed


func _on_CameraTypeSelector_item_selected(index):
	set_camera_type(index)


func _on_DefaultGravitySelector_item_selected(index):
	set_default_gravity(index)


func _on_SceneOptionSelector_item_selected(index):
	set_scene_options(index)

func GenerateCustomScene():
	if (current_perspective == null or current_camera_type == null or current_default_gravity == null or current_scene_option == null) and ($CameraTypeSelector.visible and $DefaultGravitySelector.visible):
		#Options not properly selected
		$MessageLabel.text = "Error: The scene could not be created because not all options were specified."
		return false
	else:
		#Put the code in here to actually generate the scene
		$MessageLabel.text = "Success! The scene was created - view it in the scene view (2D or 3D at the top of the screen)."
		#Actually switch to the scene
		return true


func _on_GenerateSceneButton_pressed():
	GenerateCustomScene()
